package mqtt;

	import org.eclipse.paho.client.mqttv3.MqttException;

import com.phidget22.PhidgetException;
import com.phidget22.RFID;
import com.phidget22.RFIDTagEvent;
import com.phidget22.RFIDTagListener;
import com.phidget22.RFIDTagLostEvent;
import com.phidget22.RFIDTagLostListener;
import com.phidget22.VoltageRatioInput;
import com.phidget22.VoltageRatioInputVoltageRatioChangeEvent;
import com.phidget22.VoltageRatioInputVoltageRatioChangeListener;

import mqtt.publisher.PhidgetPublisher;

	public class RFIDDoorOpener  {
	    
	    PhidgetPublisher publisher = new PhidgetPublisher(); // source in PhidgetPublisher.java
        VoltageRatioInput sensor = new VoltageRatioInput();
        
        RFID rfid = new RFID();;
	    
	    public static void main(String[] args) throws PhidgetException {
	        new RFIDDoorOpener();
	    }

	    public RFIDDoorOpener() throws PhidgetException {

	        try {
	            // OPTIONAL This is the id of your PhidgetInterfaceKit (on back of device)
	            // sensor.setDeviceSerialNumber(319864);
	            // This is the channel your slider is connected to on the interface kit
	            sensor.setChannel(7);
	            sensor.open(5000);
	            sensor.addVoltageRatioChangeListener(new VoltageRatioInputVoltageRatioChangeListener() {
	      			public void onVoltageRatioChange(VoltageRatioInputVoltageRatioChangeEvent e) {
	      				double sensordata = e.getVoltageRatio();
	      				System.out.println("Sensor Voltage Ratio Changed: "+ sensordata); 
	      				// Publish the sensor data via MQTT
	      				// Code to publish is in file PhidgetPublisher.java
						try {
							publisher.publishSlider(sensordata);
						} catch (MqttException mqtte) {
							mqtte.printStackTrace();
						}						
	      			}
	             });

	        	// Make the RFID Phidget able to detect loss or gain of an rfid card
	            rfid.addTagListener(new RFIDTagListener() {
	    			public void onTag(RFIDTagEvent e) {
	    				String tagStr = e.getTag();
	    				System.out.println("Tag read: " + tagStr);
	    				try {
	    					if (tagStr.equals("1600ee15e9")) { 
	    						publisher.publishRfid(tagStr);
	    						publisher.publishMotor(tagStr);
	    					}
						} catch (MqttException mqtte) {
							mqtte.printStackTrace();
						}	
	    			}
	            });

	            rfid.addTagLostListener(new RFIDTagLostListener() {
	    			public void onTagLost(RFIDTagLostEvent e) {
	    				String tagStr = e.getTag();
	    				System.out.println("Tag lost: " + tagStr);
	    			}
	            });
	            // Open and start detecting rfid cards
	            rfid.open(5000);  // wait 5 seconds for device to respond

	            // Display info on currently connected devices
	            System.out.println("Device Name " + rfid.getDeviceName());
	            System.out.println("Serial Number " + rfid.getDeviceSerialNumber());
	            System.out.println("Device Version " + rfid.getDeviceVersion());


	            rfid.setAntennaEnabled(true);

	            
	            System.out.println("\n\nGathering data for 15 seconds\n\n");
	            try {
	    			Thread.sleep(15000);
	    		} catch (InterruptedException e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		}

	            rfid.close();
	            System.out.println("\nClosed RFID");

	            // Start the main code
	            double x;
	              x = sensor.getVoltageRatio();
	            System.out.println("Start sensor Voltage Ratio is "+x);

	            // attach to the sensor and start reading
	            try {      	                                
	                System.out.println("\n\nGathering data for 15 seconds\n\n");
	                Thread.sleep(15000);
	            } catch (Exception ex) {
	                ex.printStackTrace();
	            }
	        } finally {
	            sensor.close();
	            System.out.println("Closed and exiting...");
	        }
	    }	
}