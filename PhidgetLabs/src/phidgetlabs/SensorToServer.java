package phidgetlabs;
import java.io.BufferedReader;

import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.google.gson.Gson;
import com.phidget22.*;

public class SensorToServer  {
    
	SensorData oneSensor = new SensorData("unknown", "unknown", "15064646" );

	Gson  gson;
	
	//String oneSensorJson;
    VoltageRatioInput slider = new VoltageRatioInput();
    VoltageRatioInput light = new VoltageRatioInput();
    int lastSensorValue = 0;
    
    // Declare String to hold json representation of sensor object data
    String oneSensorJson = new String();
    
    // address of server which will receive sensor data
    //public static String sensorServerURL = "http://localhost:8080/PhidgetServer/SensorServer";
	// Adding the username and password of mysql
	String user = "rashidd";
	String password = "rooSedef6";
	// The url which is sued for the mudfoot server and usingg port, 6306.
	String sensorServerURL = "jdbc:mysql://mudfoot.doc.stu.mmu.ac.uk:6306/"+user;
	
     public static void main(String[] args) throws PhidgetException {
        new SensorToServer();
     }
     
    public SensorToServer() throws PhidgetException {
   	
       // This is the id of your PhidgetInterfaceKit (on back of device)
        // slider.setDeviceSerialNumber(319864);
        // This is the channel your slider is connected to on the interface kit
    	 light.setChannel(0);
         light.open(1000);
         
         light.addVoltageRatioChangeListener(new VoltageRatioInputVoltageRatioChangeListener() {
   			public void onVoltageRatioChange(VoltageRatioInputVoltageRatioChangeEvent e) {
 		double sensorReading = e.getVoltageRatio();
			//System.out.println("Slider Voltage Ratio Changed: "+ sensorReading);
			
			// scale the sensor value from 0-1 to 0-1000
			int scaledSensorReading = (int) (1000 * sensorReading);
			// send value to server if changed since last reading
			if (scaledSensorReading != lastSensorValue ) {
				System.out.println("Sending new light value : " + scaledSensorReading);
				// Change sensor value to String and send to server
				String strSensorReading = "" + scaledSensorReading;
			       sendToServer(strSensorReading);
			       lastSensorValue = scaledSensorReading;
				}
   			}
         });
    	
        slider.setChannel(0);
        slider.open(5000);
        slider.addVoltageRatioChangeListener(new VoltageRatioInputVoltageRatioChangeListener() {
  			public void onVoltageRatioChange(VoltageRatioInputVoltageRatioChangeEvent e) {
  				double sensorReading = e.getVoltageRatio();
  				//System.out.println("Slider Voltage Ratio Changed: "+ sensorReading);
  				
  				// scale the sensor value from 0-1 to 0-1000
  				int scaledSensorReading = (int) (1000 * sensorReading);
  				// send value to server if changed since last reading
  				if (scaledSensorReading != lastSensorValue ) {
  					System.out.println("Sending new sensor value : " + scaledSensorReading);
  					// Change sensor value to String and send to server
  					String strSensorReading = "" + scaledSensorReading;
  					
  					strSensorReading = new Gson().toJson(oneSensor); 
  					
  					sendToServer(strSensorReading);
  					
  					lastSensorValue = scaledSensorReading;
  					
  					oneSensorJson = gson.toJson(oneSensor);
  					sendToServer(oneSensorJson);
  				}
  			}
         });
         // attach to the sensor and start reading
       // try {                      
            System.out.println("\n\nGathering data for 15 seconds\n\n");
           while (true); 
           // slider.close();
          //  System.out.println("\nClosed slider Voltage Ratio Input");
            
        /*} catch (PhidgetException ex) {
            System.out.println(ex.getDescription());
        }*/

    }
    
    //SUCCESSFUL SLIDER FUNCTION CODE START 

    public String sendToServer(String sensorValue){
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String fullURL = sensorServerURL + "?sensorname=slider&sensorvalue="+sensorValue;
        System.out.println("Sending data to: "+fullURL);  // DEBUG confirmation message
        String line;
        String result = "";
        try {
           url = new URL(fullURL);
           conn = (HttpURLConnection) url.openConnection();
           conn.setRequestMethod("GET");
           rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
           // Request response from server to enable URL to be opened
           while ((line = rd.readLine()) != null) {
              result += line;
           }
           rd.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
        return result;    	
    }
  //SUCCESSFUL SLIDER FUNCTION CODE START
    
    public String sendToServer1(String oneSensorJson){
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        // Replace invalid URL characters from json string
        try {
			oneSensorJson = URLEncoder.encode(oneSensorJson, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
        String fullURL = sensorServerURL + "?sensordata="+oneSensorJson;
        System.out.println("Sending data to: "+fullURL);  // DEBUG confirmation message
        String line;
        String result = "";
        try {
           url = new URL(fullURL);
           conn = (HttpURLConnection) url.openConnection();
           conn.setRequestMethod("GET");
           rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
           // Request response from server to enable URL to be opened
           while ((line = rd.readLine()) != null) {
              result += line;
           }
           rd.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
        return result;    	
    }
    
    
    

    //LIGHT SENSOR FUNCTION CODE START 

    public String sendToServer2(String sensorValue){
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String fullURL = sensorServerURL + "?sensorname=Light&sensorvalue="+sensorValue;
        System.out.println("Sending data to: "+fullURL);  // DEBUG confirmation message
        String line;
        String result = "";
        try {
           url = new URL(fullURL);
           conn = (HttpURLConnection) url.openConnection();
           conn.setRequestMethod("GET");
           rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
           // Request response from server to enable URL to be opened
           while ((line = rd.readLine()) != null) {
              result += line;
           }
           rd.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
        return result;    	
    }
  //LIGHT SENSOR FUNCTION CODE START
	private void pause(int secs){
        try {
			Thread.sleep(secs*1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

}