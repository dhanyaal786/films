package phidgetlabs;

import java.util.ArrayList;
import java.util.Arrays;

import com.phidget22.*;


public class RFIDExample1 {

    public static void main(String[] args) throws PhidgetException {
        new RFIDExample1();
    }

    public RFIDExample1() throws PhidgetException {

 
    	RFID phid = new RFID();
    		
    	// Make the RFID Phidget able to detect loss or gain of an rfid card
        phid.addTagListener(new RFIDTagListener() {
        		// What to do when a tag is found
			public void onTag(RFIDTagEvent e) {
				System.out.println("Tag read: " + e.getTag());
				
				if (e.getTag().equals("hello"))
				{
					System.out.print("	Welcome back "+e.getTag());
				}
				
				
				try {
					System.out.println("			"+e.getSource().getDeviceID());
				} catch (PhidgetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				
				// Create a list of tag items that are recognised	
				final String[] tagItemsArray = {"hello","tagid2","tagid3"};
			ArrayList tagItemsList = new ArrayList<String>(Arrays.asList(tagItemsArray));
			if (tagItemsList.contains(e.getTag()))
			{
				System.out.println("	"+e.getTag()+"You work in the office COME IN!");
			}
				
			}
        });

        phid.addTagLostListener(new RFIDTagLostListener() {
        		  // What to do when a tag is lost
			public void onTagLost(RFIDTagLostEvent e) {
				System.out.println("Tag Removed from sensor: " + e.getTag());
			}
        });
        // Open and start detecting rfid cards
        phid.open(5000);  // wait 5 seconds for device to respond

        // Display info on currently connected devices
        System.out.println("Device Name " + phid.getDeviceName());
        System.out.println("Serial Number " + phid.getDeviceSerialNumber());
        System.out.println("Device Version " + phid.getDeviceVersion());

        phid.setAntennaEnabled(true);

        System.out.println("\n\nGathering data for 15 seconds\n\n");
        try {
			Thread.sleep(15000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        phid.close();
        System.out.println("\nClosed RFID");
}
}